import "moment/locale/fr";
import "numeral/locales/fr";
import "bootstrap-datepicker/js/locales/bootstrap-datepicker.fr";
import "./../js/lib/pivotTable/i18n/pivot.fr";
import "./../js/lib/highcharts/i18n/highcharts.fr";
import "bootstrap-markdown/locale/bootstrap-markdown.fr";