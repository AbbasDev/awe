import "moment/locale/es";
import "numeral/locales/es";
import "bootstrap-datepicker/js/locales/bootstrap-datepicker.es";
import "./../js/lib/pivotTable/i18n/pivot.es";
import "./../js/lib/highcharts/i18n/highcharts.es";
import "bootstrap-markdown/locale/bootstrap-markdown.es";